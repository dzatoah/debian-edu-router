# Croatian translations for PACKAGE package.
# Copyright (C) 2022 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-12 12:39+0200\n"
"PO-Revision-Date: 2022-04-20 15:46+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../bin/debian-edu-router-loginmenu.sh:33
#, sh-format
msgid "Welcome to %s %s (%s) on %s"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:34
#, sh-format
msgid "WARNING: All networks are currently down. Check your configuration."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:35
#, sh-format
msgid "machine ID"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:37
#, sh-format
msgid "%s Menu"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:38
#: ../bin/debian-edu-router-loginmenu.sh:49
#, sh-format
msgid "%s Configuration"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:39
#, sh-format
msgid "Launch a shell session"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:40
#, sh-format
msgid "IP traffic statistics"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:41
#, sh-format
msgid "Quit menu and logout as user '%s'"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:42
#, sh-format
msgid "Please select: "
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:44
#, sh-format
msgid "Starting IPTRAF traffic monitor..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:45
#, sh-format
msgid "Starting shell '%s'..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:46
#, sh-format
msgid "Entering %s configuration submenu..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:47
#, sh-format
msgid "Quit"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:50
#, sh-format
msgid "Configure %s network settings"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:51
#, sh-format
msgid "Configure %s firewall settings"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:52
#, sh-format
msgid "Configure %s services"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:54
#, sh-format
msgid "Configure networking..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:55
#, sh-format
msgid "Configure firewall settings..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:56
#, sh-format
msgid "Configure services..."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:57
#, sh-format
msgid "Back to main menu"
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:59
#, sh-format
msgid "Return back to main menu"
msgstr ""
