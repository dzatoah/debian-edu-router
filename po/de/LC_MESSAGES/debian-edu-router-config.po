# German translations for PACKAGE package.
# Copyright (C) 2022 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Mike Gabriel <mike.gabriel@das-netzwerkteam.de>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: 2.12.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-12 12:39+0200\n"
"PO-Revision-Date: 2022-05-09 14:05+0200\n"
"Last-Translator: Mike Gabriel <mike.gabriel@das-netzwerkteam.de>\n"
"Language-Team: German <translation-team-de@lists.sourceforge.net>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.4.2\n"

#: ../bin/debian-edu-router-loginmenu.sh:33
#, sh-format
msgid "Welcome to %s %s (%s) on %s"
msgstr "Willkommen bei %s %s (%s) auf %s"

#: ../bin/debian-edu-router-loginmenu.sh:34
#, sh-format
msgid "WARNING: All networks are currently down. Check your configuration."
msgstr ""

#: ../bin/debian-edu-router-loginmenu.sh:35
#, sh-format
msgid "machine ID"
msgstr "Maschinen-ID"

#: ../bin/debian-edu-router-loginmenu.sh:37
#, sh-format
msgid "%s Menu"
msgstr "%s Menü"

#: ../bin/debian-edu-router-loginmenu.sh:38
#: ../bin/debian-edu-router-loginmenu.sh:49
#, sh-format
msgid "%s Configuration"
msgstr "%s Konfiguration"

#: ../bin/debian-edu-router-loginmenu.sh:39
#, sh-format
msgid "Launch a shell session"
msgstr "Eine Shell-Sitzung starten"

#: ../bin/debian-edu-router-loginmenu.sh:40
#, sh-format
msgid "IP traffic statistics"
msgstr "IP-Statistik starten"

#: ../bin/debian-edu-router-loginmenu.sh:41
#, sh-format
msgid "Quit menu and logout as user '%s'"
msgstr "Menu verlassen und Benutzer '%s' abmelden."

#: ../bin/debian-edu-router-loginmenu.sh:42
#, sh-format
msgid "Please select: "
msgstr "Bitte auswählen: "

#: ../bin/debian-edu-router-loginmenu.sh:44
#, sh-format
msgid "Starting IPTRAF traffic monitor..."
msgstr "Starte nun den IPTRAF Traffic Monitor..."

#: ../bin/debian-edu-router-loginmenu.sh:45
#, sh-format
msgid "Starting shell '%s'..."
msgstr "Starte Kommandozeileninterpreter '%s'..."

#: ../bin/debian-edu-router-loginmenu.sh:46
#, sh-format
msgid "Entering %s configuration submenu..."
msgstr "Öffne das %s Konfigurationsuntermenü..."

#: ../bin/debian-edu-router-loginmenu.sh:47
#, sh-format
msgid "Quit"
msgstr "Beenden"

#: ../bin/debian-edu-router-loginmenu.sh:50
#, sh-format
msgid "Configure %s network settings"
msgstr "%s Netzwerkkonfiguration starten"

#: ../bin/debian-edu-router-loginmenu.sh:51
#, sh-format
msgid "Configure %s firewall settings"
msgstr "%s Firewall Konfiguration starten"

#: ../bin/debian-edu-router-loginmenu.sh:52
#, sh-format
msgid "Configure %s services"
msgstr "%s Dienste konfigurieren"

#: ../bin/debian-edu-router-loginmenu.sh:54
#, sh-format
msgid "Configure networking..."
msgstr "Netzwerk konfigurieren..."

#: ../bin/debian-edu-router-loginmenu.sh:55
#, sh-format
msgid "Configure firewall settings..."
msgstr "Firewall konfigurieren..."

#: ../bin/debian-edu-router-loginmenu.sh:56
#, sh-format
msgid "Configure services..."
msgstr "Dienste konfigurieren..."

#: ../bin/debian-edu-router-loginmenu.sh:57
#, sh-format
msgid "Back to main menu"
msgstr "Wieder zum Hauptmenü"

#: ../bin/debian-edu-router-loginmenu.sh:59
#, sh-format
msgid "Return back to main menu"
msgstr "Zurück zum Hauptmenü"

#, sh-format
#~ msgid "Quit menu"
#~ msgstr "Menü beenden"

#, sh-format
#~ msgid "%s Setup"
#~ msgstr "%s Konfiguration"

#, sh-format
#~ msgid "Debian Edu Router Menu\\n======================"
#~ msgstr "Debian Edu Router Menü\\n======================"

#, sh-format
#~ msgid "Debian Edu Router Setup"
#~ msgstr "Debian Edu Router Einrichtung"
