Default: d-e-r::en_US

Name: d-e-r::en_US
Description: Debian Edu Router - Englisch
Short: Debian Edu Router with English keyboard layout
Long: This installs the Debian Edu Router system with an English (en_US) keyboard layout.
Classes: INSTALL FAIBASE DEBIAN DEBIAN_EDU_ROUTER LVM_EDU_ROUTER GATEWAY FIREWALL DNS_SERVER NTP_SERVER PROXY WEBFILTER

Name: d-e-r::de_DE
Description: Debian Edu Router - Deutsch
Short: Debian Edu Router mit deutschem Tastaturlayout
Long: Dies installiert das Debian Edu Router System mit deutschem (de_DE) Tastaturlayout.
Classes: INSTALL FAIBASE DEBIAN DEBIAN_EDU_ROUTER LVM_EDU_ROUTER GATEWAY FIREWALL DNS_SERVER NTP_SERVER PROXY WEBFILTER LANG_GERMAN
