#! /bin/bash

# Copyright (C) 2002-2013 Thomas Lange <lange@informatik.uni-koeln.de>
# Copyright (C) 2022 Pädagogisches Landesinstitut Rheinland-Pfalz
# Copyright (C) 2022 Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
# Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

# NOTE: Files named *.sh will be evaluated, but their output ignored.

[ $do_init_tasks -eq 1 ] || return 0 # Do only execute when doing install

echo 0 > /proc/sys/kernel/printk

#kernelmodules=
# here, you can load modules depending on the kernel version
case $(uname -r) in
    2.6*) kernelmodules="$kernelmodules mptspi dm-mod md-mod aes dm-crypt" ;;
    [3456]*) kernelmodules="$kernelmodules mptspi dm-mod md-mod aes dm-crypt" ;;
esac

for mod in $kernelmodules; do
    [ X$verbose = X1 ] && echo Loading kernel module $mod
    modprobe -a $mod 1>/dev/null 2>&1
done

# show the basic information about the network interface
ip -br li show up|egrep -v ^lo; ip -br a show up|egrep -v ^lo

echo $printk > /proc/sys/kernel/printk

odisklist=$disklist
set_disk_info  # recalculate list of available disks
if [ "$disklist" != "$odisklist" ]; then
    echo New disklist: $disklist
    echo disklist=\"$disklist\" >> $LOGDIR/additional.var
fi

save_dmesg     # save new boot messages (from loading modules)
